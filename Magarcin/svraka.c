#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

const char *usage =
"1. Formiranje datoteke\n"
"2. Dodavanje artikla\n"
"3. Izdavanje artikla\n"
"4. Pregled jednog artikla\n"
"5. Pregled celog magarcina\n"
"6. Izlaz";

struct article{
    int amount, price;
    char *name;
};

struct article* create_article(int amount, int price, const char *name){
    struct article *tmp_art = malloc(sizeof(struct article));
    tmp_art->amount = amount;
    tmp_art->price = price;
    int n = strlen(name);
    tmp_art->name = malloc(n+1);
    strcpy(tmp_art->name, name);
    return tmp_art;
}

void destroy_article(struct article *art){
    free(art->name);
    free(art);
}

struct array{
    int nsize, size;
    struct article **a;
};

struct array* array_create(){
	struct array *tmp = calloc(1, sizeof(struct array));
	return tmp;
}

void array_push(struct array *arr, const struct article *art){
    if(arr->size+1 >= arr->nsize){
        if(arr->size == 0){
            arr->a = malloc(sizeof(struct article*)*16);
            arr->nsize = 16;
        }else{
            arr->nsize *= 2;
            arr->a = realloc(arr->a, arr->nsize*sizeof(struct article*));
        }
    }
    arr->a[arr->size++] = art;
}

void array_destroy(struct array *arr){
    for(int i = 0; i < arr->size; ++i) destroy_article(arr->a[i]);
    free(arr);
}

void output(const char *str, int n, FILE *bsf){
    for(int i = 0; i < n; ++i){
        fputc(str[i], bsf);
    }
}

char xor_key;
void dencrypt(char *str, int n){
    for(int i = 0; i < n; ++i){
        str[i] ^= xor_key;
    }
}

void load_new(FILE *bsf){
    char art[256];
    int amount, price;
    fputc(xor_key, bsf);
    for(;;){
        printf("Ime artikla: "); scanf("%s", art);
        if(!strcmp(art, "done")) return;
        printf("Kolicina artikla: "); scanf("%d", &amount);
        printf("Cena artikla: "); scanf("%d", &price);

        char final_article[512];
        int n = sprintf(final_article, "%s %d %d ", art, amount, price);
        dencrypt(final_article, n);
        output(final_article, n, bsf);
    }
}

void get_key(const char *filename){
    FILE *bsf = fopen(filename, "rb");
    xor_key = fgetc(bsf);
    fclose(bsf);
}

char* load_file(FILE *bsf){
    bsf = freopen(NULL, "rb", bsf);
    
    char *buff = calloc(64*1024*1024, 1);
    int n = fread(buff, 1, 64*1024*1024, bsf);

    char xor_key = buff[0];
    dencrypt(buff, n);
	buff[0] = ' ';
    return buff;
}

struct array* to_array(const char *buff){
	char *str = strdup(buff);
	char *token = strtok(str, " ");

	int cur = 0;
	char *name = NULL;
	int amount, price;
	struct array *arr = array_create();
	while(token){
		switch(cur){
		case 0:
			name = strdup(token);
			break;
		case 1:
			amount = atoi(token);
			break;
		case 2:
			price = atoi(token);
			break;
		}
		
		++cur;
		if(cur == 3){
			const struct article *art = create_article(amount, price, name);
			array_push(arr, art);
			cur = 0;
		}
		token = strtok(NULL, " ");
	}
	return arr;
}

struct article* array_find(const struct array *arr, const char *name){
	for(int i = 0; i < arr->size; ++i){
		if(!strcmp(arr->a[i]->name, name)) return arr->a[i];
	}
	return NULL;
}

void array_output(const struct array *arr, FILE *bsf){
	char *buff = calloc(64*1024*1024, 1);
	char *cur = buff;
	int len = 0;
	for(int i = 0; i < arr->size; ++i){
		int n = sprintf(cur, "%s %d %d ", arr->a[i]->name, arr->a[i]->amount, arr->a[i]->price);
		cur += n;
		len += n;
	}
	dencrypt(buff, len);
	fputc(xor_key, bsf);
	output(buff, len, bsf);
}

void add_new(FILE *bsf){
    char name[256];
    int amount, price;
    printf("Ime artikla: "); scanf("%s", name);
    printf("Kolicina artikla: "); scanf("%d", &amount);

    char *buff = load_file(bsf);
    struct array *arr = to_array(buff+1);
	
	struct article *art = array_find(arr, name);
	if(art){
		art->amount += amount;
	}else{
		printf("Cena artikla: "); scanf("%d", &price);
		art = create_article(amount, price, name);
		array_push(arr, art);
	}

	freopen(NULL, "wb", bsf);
	array_output(arr, bsf);

	free(buff);
	array_destroy(arr);
}

void remove_one(FILE *bsf){
	char name[256];
    int amount, price;
    printf("Ime artikla: "); scanf("%s", name);
    printf("Kolicina artikla: "); scanf("%d", &amount);

    char *buff = load_file(bsf);
    struct array *arr = to_array(buff+1);
	
	struct article *art = array_find(arr, name);
	if(!art){
		printf("Artikla nema u magarcinu. Steta\n");
		free(buff);
		array_destroy(arr);
		return;
	}
	if(art->amount < amount){
		printf("Artikla nema toliko, ima ga samo %d!\n", art->amount);
		free(buff);
		array_destroy(arr);
		return;
	}
	art->amount -= amount;
	
	freopen(NULL, "wb", bsf);
	array_output(arr, bsf);

	free(buff);
	array_destroy(arr);
}

void show(int f_all, FILE *bsf){
	char name[256];
    int amount, price;
    if(!f_all) printf("Ime artikla: "); scanf("%s", name);

    char *buff = load_file(bsf);
    struct array *arr = to_array(buff+1);

	if(!arr){
		printf("Artikla nema u magarcinu. Steta\n");
		free(buff);
		array_destroy(arr);
		return ;
	}

	if(!f_all){
		struct article *art = array_find(arr, name);
		printf("(kolicina, cena, vrednost): %d %d %d\n", art->amount,
			   art->price, art->amount*art->price);
	}else{
		for(int i = 0; i < arr->size; ++i){
			printf("%s(kolicina, cena, vrednost): %d %d %d\n", arr->a[i]->name, arr->a[i]->amount,
				   arr->a[i]->price, arr->a[i]->amount*arr->a[i]->price);
		}
	}

	free(buff);
	array_destroy(arr);
}

int main(int argc, char **argv){
	const char *filepath = "magarcin.enc";
    FILE *bsf;
    srand(time(NULL));
	printf("\33[2J\33[H");
    for(;;){
		puts(usage);
        int cmd; scanf("%d", &cmd);
		printf("\33[2J\33[H");
        switch(cmd){
            case 1:
                bsf = fopen(filepath, "wb");
                xor_key = rand()%256;
                load_new(bsf);
                fclose(bsf);
                break;
            case 2:
                get_key(filepath);
                bsf = fopen(filepath, "rb");
                add_new(bsf);
                fclose(bsf);
                break;
            case 3:
				get_key(filepath);
				bsf = fopen(filepath, "rb");
				remove_one(bsf);
				fclose(bsf);
                break;
            case 4:
				get_key(filepath);
				bsf = fopen(filepath, "rb");
				show(0, bsf);
				fclose(bsf);
                break;
            case 5:
				get_key(filepath);
				bsf = fopen(filepath, "rb");
				show(1, bsf);
				fclose(bsf);
                break;
            case 6:
                exit(0);
        }
    }
    return 0;
}
